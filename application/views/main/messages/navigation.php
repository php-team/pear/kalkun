<?php $this->load->helper('inflector'); ?>

<div class="bttn-container">
	<?php
	if ($this->uri->segment(2) === 'conversation'):
		if ($this->uri->segment(3) === 'folder'):

			// _tni_ added this for translation on the inbox, outbox etc.
			$theFolder = $this->uri->segment(4);
			switch ($theFolder)
			{
				case 'inbox':
					$theFname = tr_raw('Inbox');
					break;
				case 'outbox':
					$theFname = tr_raw('Outbox');
					break;
				case 'sentitems':
					$theFname = tr_raw('Sent items');
					break;
				case 'phonebook':
					$theFname = tr_raw('Phonebook');
					break;
				default:
					$theFname = $this->uri->segment(4);
					break;
			}
			$anchor_url = 'messages/folder/'.$this->uri->segment(4);
			$anchor_text = '&lsaquo;&lsaquo; '.tr('Back to {0}', NULL, $theFname);
		elseif ($this->uri->segment(3) === 'my_folder'):
			$anchor_url = 'messages/my_folder/'.$this->uri->segment(4).'/'.$this->uri->segment(6);
			$anchor_text = '&lsaquo;&lsaquo; '.tr('Back to {0}', NULL, humanize($this->Kalkun_model->get_folders('name', $this->uri->segment(6))->row('name')));
		endif;
	?>
	<div class="bttn-group">
		<button><?php echo anchor($anchor_url, $anchor_text, array('class' => 'button', 'id' => 'back_threadlist'));?></button>
	</div>

	<?php endif;?>
	<div class="bttn-group">
		<button><a href="javascript:void(0);" class="select_all_button button"><?php echo tr('Select all');?></a></button>
		<button><a href="javascript:void(0);" class="clear_all_button button"><?php echo tr('Deselect all');?></a></button>
	</div>

	<div class="bttn-group">
		<?php if ($this->uri->segment(2) === 'conversation' && $this->uri->segment(4) === 'inbox') :
			if ($this->uri->segment(6) !== '6') : ?>
		<button><a href="javascript:void(0);" class="spam_button button"><?php echo tr('Report spam');?></a></button>
		<?php   else : ?>
		<button><a href="javascript:void(0);" class="ham_button button"><?php echo tr('Not spam');?></a></button>
		<?php   endif;
		endif;?>
	</div>

	<div class="bttn-group">
		<?php
	if ($this->uri->segment(2) === 'folder' && $this->uri->segment(3) === 'outbox'):
	elseif ($this->uri->segment(2) === 'conversation' && $this->uri->segment(4) === 'outbox'):
	else:?>

		<?php if ($this->uri->segment(4) === '5' or $this->uri->segment(6) === '5') : ?>
		<button><a href="javascript:void(0);" class="recover_button button"><?php echo tr('Recover');?></a></button>
		<?php endif; ?>
		<button><a class="move_to_button button" href="javascript:void(0);"><?php echo tr('Move to');?></a></button>
		<?php endif; ?>
		<button><a class="global_delete button" href="javascript:void(0);">
				<?php
	if ($this->uri->segment(4) === '5' or $this->uri->segment(6) === '5' or $this->uri->segment(4) === '6' or $this->uri->segment(6) === '6'):
		echo tr('Delete permanently');
	else:
		echo tr('Delete');
	endif;
	?></a></button>
	</div>
	<?php if ($this->uri->segment(2) !== 'search'): ?>

	<div class="bttn-group">
		<button><a href="javascript:void(0);" class="refresh_button button"><?php echo tr('Refresh');?></a></button>
		<?php if (($this->uri->segment(3) === 'inbox') || ($this->uri->segment(4) === 'inbox')): ?>
		<button><a href="javascript:void(0);" class="process_incoming_msgs_button button"><?php echo tr('Process incoming messages');?></a></button>
		<?php endif; ?>
	</div>
	<?php endif; ?>

	<div class="bttn-group">
		<?php if ($this->uri->segment(2) === 'conversation' && $this->uri->segment(4) === 'sentitems'): ?>

		<button><a href="javascript:void(0);" class="resend_bulk button"><?php echo tr('Resend');?></a></button>
		<?php endif; ?>
	</div>

	<?php if ($pagination_links !== ''): ?>
	<div class="paging">
		<div id="paging"><?php  echo $pagination_links;?></div>
	</div>
	<?php endif; ?>

</div>
