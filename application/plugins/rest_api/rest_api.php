<?php
/**
* Plugin Name: REST API
* Plugin URI: http://azhari.harahap.us
* Version: 0.1
* Description: REST API Plugin
* Author: Azhari Harahap
* Author URI: http://azhari.harahap.us
*/

class Rest_api_plugin extends CI3_plugin_system {

	use plugin_trait;
}
