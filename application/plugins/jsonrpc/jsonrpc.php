<?php
/**
* Plugin Name: JSONRPC
* Plugin URI: http://azhari.harahap.us
* Version: 1.0
* Description: JSONRPC Server Plugin
* Author: Azhari Harahap
* Author URI: http://azhari.harahap.us
*/

class Jsonrpc_plugin extends CI3_plugin_system {

	use plugin_trait;
}
