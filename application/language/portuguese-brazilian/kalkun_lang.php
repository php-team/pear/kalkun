<?php
// Brazilian Portuguese Translation
// Author: Helder Santana <helder.bs.santana@gmail.com>

// Global
$lang['Dashboard'] = 'Painel';
$lang['Folders'] = 'Pastas';
$lang['My folders'] = 'Minhas Pastas';

$lang['Compose'] = 'Escrever';
$lang['Inbox'] = 'Caixa de Entrada';
$lang['Outbox'] = 'Caixa de Saida';
$lang['Sent items'] = 'Enviadas';
$lang['Sending error'] = 'Erros de Envio';
$lang['Trash'] = 'Lixeira';
$lang['Spam'] = 'SPAM';
$lang['Add'] = 'Adicionar';
$lang['Action'] = 'Acao';

$lang['Phonebook'] = 'Agenda';
$lang['Settings'] = 'Preferencias';
$lang['Filters'] = 'Filtros';
$lang['About {0}'] = 'Sobre {0}';
$lang['Logout'] = 'Sair';

// Dashboard
$lang['Statistics'] = 'Estatisticas';
$lang['System information'] = 'Informações do Sistema';
$lang['Alerts'] = 'Alertas';
$lang['Operating system'] = 'Sistema Operacional';
$lang['Gammu version'] = 'Versão Gammu';
$lang['Gammu DB schema'] = 'Gammu DB';
$lang['Modem IMEI'] = 'IMEI';

// Phonebook
$lang['Contact'] = 'Contato';
$lang['All contacts'] = 'Todos os Contatos';
$lang['Send to all contacts'] = 'Enviar a todos';
$lang['Add to group'] = 'Adicionar ao Grupo';
$lang['Delete from group'] = 'Remover do Grupo';
$lang['Select add contact method'] = 'Escolha um metodo para adicionar contato';
$lang['Manual input'] = 'Usar formulario de contato';
$lang['Manually add contact using contact form'] = 'Adicionar manualmente utilizando formulario';
$lang['From CSV file'] = 'Importar arquivo CSV';
$lang['Import contact from CSV format file'] = 'Importar contatos de arquivo CSV';
$lang['CSV file'] = 'Arquivo CSV';
$lang['The CSV file must be in valid format'] = 'O arquivo CSV precisa estar num formato valido';
$lang['Are you sure you want to delete {0} contact(s)?'] = 'Tem certeza que deseja apagar {0} contato(s)?';
$lang['valid example'] = 'Exemplo valido:';

// Message
$lang['Message'] = 'Mensagem';
$lang['Send to'] = 'Enviar Para';
$lang['Reply'] = 'Responder';
$lang['Forward'] = 'Encaminhar';
$lang['Forward to'] = 'Encaminhar para';
$lang['Move to'] = 'Mover para';
$lang['Select folder'] = 'Escolha Pasta';
$lang['Delete'] = 'Apagar';
$lang['Click to delete this folder'] = 'Clique para apagar esta pasta';
$lang['Recover'] = 'Recuperar';

$lang['Send date'] = 'Data de Envio';
$lang['Now'] = 'Agora';
$lang['At date and time'] = 'Na data e hora';
$lang['After a delay'] = 'Daqui a...';

$lang['Input manually'] = 'Digitar número';

$lang['Add folder'] = 'Adicionar Pasta';
$lang['Folder name'] = 'Nome de Pasta';
$lang['Cancel'] = 'Cancelar';
$lang['Save'] = 'Salvar';
$lang['Rename'] = 'Renomear';
$lang['Rename folder'] = 'Renomear Pasta';
$lang['Click to rename this folder'] = 'Clique para renomear esta Pasta';

$lang['This folder and all messages in it will be deleted permanently and cannot be recovered. Are you sure?'] = 'Esta pasta e mensagens serao permanentemente apagadas e nao serao recuperadas. Tem certeza?';

$lang['There is no message in {0}.'] = 'Não ha mensagens na {0}.';
$lang['There is no message in this folder.'] = 'Não ha mensagens nesta pasta.';

$lang['Delete all'] = 'Apagar tudo';
$lang['Are you sure? This will affect all conversations.'] = 'Tem certeza? Isto vai afetar todas as mensagens.';

$lang['Select all'] = 'Selecionar tudo';
$lang['Deselect all'] = 'Limpar tudo';
$lang['Refresh'] = 'Atualizar';

$lang['Report spam'] = 'É Spam';
$lang['Not spam'] = 'Não e Spam';


$lang['SMSC'] = 'SMSC';
$lang['Part'] = 'Parte';
$lang['Validity'] = 'Validade';
$lang['default'] = 'default';
$lang['5 minutes'] = '5 minutos';
$lang['10 minutes'] = '10 minutos';
$lang['30 minutes'] = '30 minutos';
$lang['1 hour'] = '1 hora';
$lang['2 hours'] = '2 horas';
$lang['4 hours'] = '4 horas';
$lang['12 hours'] = '12 horas';
$lang['1 day'] = '1 dia';
$lang['2 days'] = '2 dias';
$lang['5 days'] = '5 dias';
$lang['1 week'] = '1 semana';
$lang['2 weeks'] = '2 semanas';
$lang['4 weeks'] = '4 semanas';
$lang['maximum'] = 'maximo';

$lang['SMS type'] = 'Tipo de SMS';
$lang['Normal'] = 'Normal';
$lang['Flash'] = 'Flash';
$lang['WAP push link'] = 'Link WAP Push';
$lang['URL'] = 'URL';

$lang['Ads is active'] = 'Propagandas ativas';
$lang['Check DND'] = 'Não Perturbe';
$lang['Insert'] = 'Inserir';

$lang['Canned responses'] = 'Respostas prontas';
$lang['There are no canned responses. Continue to save your present message as canned response.'] = 'Nao há respostas prontas. Continue salvando sua mensagem atual como resposta automatica.';
$lang['Choose response'] = 'Escolher Resposta';
$lang['Saving...'] = 'Salvando...';
$lang['Are you sure?'] = 'Tem certeza?';

// Others
$lang['Amount'] = 'Quantidade';
$lang['times']['repetition'] = 'vezes';
$lang['Hour(s)'] = 'Hora';
$lang['Minutes'] = 'Minutos';
$lang['No result.'] = 'Sem resultados.';
$lang['See conversation'] = 'Ver conversa';

$lang['Search'] = 'Busca';
$lang['Advanced search'] = 'Busca Avancada';
$lang['Folder'] = 'Pasta';
$lang['All'] = 'Todas';
$lang['Date from'] = 'Data De';
$lang['Date to'] = 'Data Para';
$lang['Status'] = 'Status';
$lang['Paging'] = 'Paginacao';
$lang['{0} per page'] = '{0} por Pagina';

$lang['Resend'] = 'Reenviar';
$lang['Public contacts'] = 'Contatos Publicos';
$lang['Set as public contact'] = 'Definir como Contato Publico';
$lang['My contacts'] = 'Meus Contatos';
$lang['Public groups'] = 'Grupos Publicos';
$lang['Set as public group'] = 'Definir como grupo publico';
$lang['My groups'] = 'Meus Grupos';
$lang['Incoming SMS'] = 'SMS Recebidos';
$lang['Outgoing SMS'] = 'SMS Enviados';
$lang['Delete all messages now'] = 'Apagar todas as mensagens';
$lang['Delete contact(s) confirmation'] = 'Confirmacao para apagar contatos';
$lang['Should be a valid URL'] = 'Deve ser uma URL valida';
$lang['Import from file'] = 'Importar de arquivo';

// Messages controller
$lang['Outgoing SMS disabled.'] = 'Outgoing SMS disabled.';
$lang['A number was found in DND Resitry. SMS sending was skipped for it.'] = 'A number was found in DND Resitry. SMS sending was skipped for it.';
$lang['Message delivered successfully to user inbox.'] = 'Message successfully delivered to user inbox.';
$lang['Copy of the message was placed in the outbox and is ready for delivery.'] = 'Your message has been moved to outbox and is ready for delivery.';
$lang['No number found. SMS not sent.'] = 'No number found. SMS not sent.';
$lang['Only administrators can permanently delete messages.'] = 'Only administrators can permanently delete messages.';

// Setting
$lang['Current password'] = 'Senha atual';
$lang['Forgot your password?'] = 'Esqueceu sua senha?';
$lang['New password'] = 'Nova Senha';
$lang['Must be at least 6 characters long'] = 'Deve ter pelo menos 6 caracteres';

$lang['Enter your new password'] = 'Digite a nova senha';

$lang['Administrator'] = 'Administrador';
$lang['User']['credentials'] = 'Usuario';

$lang['Create a new filter'] = 'Criar novo filtro';
$lang['Has the words'] = 'Contem as palavras';

// SMS Content/Member
$lang['Member'] = 'Membro';
$lang['Total member'] = 'Total de membros';
$lang['There is no registered member yet'] = 'Ainda não há membros registrados';

// tni contribution
$lang['Add contact'] = 'Adicionar Contato';
$lang['Send message'] = 'Enviar Mensagem';
$lang['Send and repeat'] = 'Enviar e repetir';
$lang['Sending'] = 'Enviando';
$lang['Insert name from contact list'] = 'Inserir nome da lista de contatos';
$lang['Delete group(s) confirmation'] = 'Confirmacao para apagar grupo(s)';
$lang['Delete group(s)?
All their contacts will also be deleted.'] = 'Apagar Grupo(s)?
Todos os contatos nestes grupos tambem serao apagados.';
$lang['Create group'] = 'Criar Grupo';
$lang['Group name'] = 'Nome do Grupo';
$lang['Groups'] = 'Grupos';
$lang['Manage groups'] = 'Gerenciar Grupos';
$lang['Manage group'] = 'Gerenciar Grupo';
$lang['Manage contact'] = 'Gerenciar Contato';
$lang['Type group name'] = 'Digite nome do grupo';
$lang['No item selected.'] = 'Nenhum item selecionado.';
$lang['No contact selected.'] = 'Nenhum contato selecionado.';
$lang['Compose SMS'] = 'Escrever SMS';
$lang['Contacts'] = 'Contatos';
$lang['Back to {0}'] = 'Voltar para {0}';
$lang['Show details'] = 'Exibir Detalhes';
$lang['Hide details'] = 'Esconder Detalhes';
$lang['Search contacts'] = 'Pesquisar Contatos';
$lang['Search messages'] = 'Pesquisar Mensagens';
$lang['Edit'] = 'Editar';
$lang['All form fields are required.'] = 'Todos os campos são necessários.';
$lang['Name'] = 'Nome';
$lang['Telephone number'] = 'Numero de Telefone';
$lang['Email ID'] = 'Email ID';
$lang['Enable email forwarding'] = 'Ativar encaminhamento de email';
$lang['{0} remaining'] = '{0} restante';
$lang['{0} ago'] = '{0} atras';
$lang['Edit contact'] = 'Editar Contato';
$lang['Contact not found'] = 'Contato nao encontrado';
$lang['No contacts in the database.'] = 'Nenhum contato no banco de dados.';
$lang['Add user'] = 'Adicionar Usuario';
$lang['Edit user'] = 'Editar Usuario';
$lang['User']['default'] = 'Usuario';
$lang['Users'] = 'Usuarios';
$lang['This deletes the selected users and all their messages and contacts.'] = 'Isso exclui os usuários selecionados e todas as suas mensagens e contatos.';
$lang['User not found'] = 'Usuario nao encontrado';
$lang['No users in the database.'] = 'Nenhum usuario na base de dados';
$lang['Username'] = 'Username';
$lang['Password'] = 'Password';
$lang['Search user'] = 'Pesquisar Usuario';
$lang['Confirm password'] = 'Confirmar Senha';
$lang['Phone number'] = 'Numero de telefone';
$lang['Action not allowed'] = 'Acao nao permitida';
$lang['Field required.'] = 'Campo requerido.';
$lang['Passwords do not match.'] = 'Senhas não conferem.';
$lang['No user selected'] = 'Nenhum usuario selecionado';
$lang['Delete this folder'] = 'Apagar esta pasta';
$lang['Loading'] = 'Carregando';
$lang['Connected'] = 'Conectado';
$lang['Disconnected'] = 'Desconectado';
$lang['No group detected, add one first.'] = 'Nenhum grupo encontrado, primeiro crie um.';
$lang['No group selected.'] = 'Nenhum grupo selecionado.';
$lang['From'] = 'De';
$lang['To'] = 'Para';
$lang['Inserted'] = 'Inserido';
$lang['Date'] = 'Data';
$lang['Sending failed'] = 'Erro no Envio';
$lang['Sent, no report'] = 'Enviado, sem Relatorio';
$lang['Sent, waiting for report'] = 'Enviado, aguardando relatorio';
$lang['Delivered'] = 'Entregue';
$lang['Pending'] = 'Pendente';
$lang['Unknown'] = 'Desconhecido';

$lang['Country calling code'] = 'Código de chamada do pais';

$lang['Conversation sort'] = 'Organizar conversas';

$lang['Data per page'] = 'Dados por pagina';
$lang['Used for paging in message and phonebook'] = 'Usado para paginacao em mensagens e agenda telefonica';

$lang['Permanent delete'] = 'Exclusao Definitiva';
$lang['Disable'] = 'Desabilitar';
$lang['Always move to trash first'] = 'Sempre mover para lixeira';
$lang['Enable'] = 'Habilitar';

$lang['User settings'] = 'Configuracoes de usuario';
$lang['General'] = 'Geral';
$lang['Personal'] = 'Pessoal';

$lang['Signature'] = 'Assinatura';
$lang['Max. 50 characters'] = 'Max. 50 caracteres';
$lang['Signature is added at the end of the message.'] = 'A assinatura será inserida no fim da mensagem.';
$lang['Language'] = 'Idioma';
$lang['Yes'] = 'Sim';
$lang['No'] = 'Não';
$lang['Default'] = 'Default';
$lang['Delivery Report'] = 'Relatorio de entrega';

$lang['Delete copy (prevents duplicates).'] = 'Apagar copia (Previne duplicadas).';
$lang['You are about to resend {0} message(s).'] = 'Você está prestes a reenviar {0} mensagens.';

// Kalkun Controller
$lang['Outgoing SMS disabled. Contact system administrator.'] = 'SMS de saida desativado. Entre em contato com o administrador.';
$lang['Wrong password'] = 'Voce digitou uma senha errada';
$lang['Username already taken'] = 'Usuario ja existe';
$lang['Settings saved successfully.'] = 'Suas configuracoes foram salvas com sucesso.';
// Users Controller
$lang['Access denied.'] = 'Acesso negado.';
$lang['User updated successfully.'] = 'Usuario foi atualizado.';
$lang['User added successfully.'] = 'Usuario foi adicionado.';
// Pluginss Controller
$lang['Access denied. Only administrators are allowed to manage plugins.'] = 'Somente adminitradores podem gerenciar plugins.';
$lang['Plugin {0} installed successfully.'] = 'Plugin {0} instalado com sucesso.';
$lang['Plugin {0} uninstalled successfully.'] = 'Plugin {0} desinstalado com sucesso.';
$lang['Installed']['Plural'] = 'Instalado';
$lang['Available']['Plural'] = 'Disponivel';
// Phonebook Controller
$lang['{0,number,integer} contacts imported successfully.'] = '{0,number,integer} contacts successfully imported.';
$lang['Contact updated successfully.'] = 'Contato atualizado com sucesso.';
$lang['Contact added successfully.'] = 'Contato adicionado com sucesso.';
// Gammu Model
$lang['Message queued.'] = 'Mensagem na fila.';
$lang['Parameter invalid.'] = 'Parametro invalido.';

$lang['403 Forbidden'] = '403 Proibido';

$lang['Close'] = 'Fechar';
$lang['Previous'] = 'Anterior';
$lang['Next'] = 'Proximo';
$lang['Continue'] = 'Continue';
$lang['Submit']['form'] = 'Enviar';
$lang['Log in'] = 'Logar';
$lang['Username or password are incorrect.'] = 'Usuario ou senha estao incorretos.';
$lang['Token already generated and still active.'] = 'Token ja existe e permanece ativo.';
$lang['To reset your Kalkun password please visit {0}'] = 'Para resetar sua senha visite {0}';
$lang['If you are a registered user, a SMS has been sent to you.'] = 'Se voce for um usuario registrado, um SMS foi enviado para voce.';
$lang['Password changed successfully.'] = 'Senha alterada com sucesso.';
$lang['Token invalid.'] = 'Token invalido.';
$lang['Password reset'] = 'Reinicializar senha';
$lang['or'] = 'ou';
$lang['Please enter your username and password'] = 'Digite seu usuario e senha';
$lang['Remember me'] = 'Remember me';
$lang['Installation'] = 'Instalacao';
$lang['Installation steps'] = 'Passos da instalacao';
$lang['Welcome screen'] = 'Tela de boas vindas';
$lang['Requirements check'] = 'Verificacao de requisitos';
$lang['Database setup'] = 'Configuracao do banco de dados';
$lang['Final configuration steps'] = 'Etapa final de configuracao';
$lang['Ok'] = 'Ok';
$lang['Missing'] = 'Faltando';
$lang['Found'] = 'Encontrado';
$lang['Successful'] = 'Sucesso';
$lang['Failed'] = 'Falhou';
$lang['Kalkun installation assistant'] = 'Assistente de instalacao do Kalkun';
$lang['This welcome screen'] = 'Tela de boas vindas';
$lang['Database installation or upgrade'] = 'Instalacao ou atualizacao do banco de dados';
$lang['Keyboard shortcuts'] = 'atalhos de teclado';
$lang['Jumping'] = 'Pulando';
$lang['{0} then {1}:'] = '{0} entao {1}:';
$lang['Navigation'] = 'Navegacao';
$lang['Back to conversation list'] = 'Voltar para lista de conversas';
$lang['Highlight prev/next'] = 'Destacar ant/prox';
 $lang['Open prev/next (message only)'] = 'Abrir ant/prox (apenas mensagens)';
$lang['{0} or {1}:'] = '{0} ou {1}:';
$lang['Open'] = 'Abrir';
$lang['Selection'] = 'Selecao';
$lang['Select'] = 'Selecione';
$lang['Actions'] = 'Acoes';
$lang['Move selected'] = 'Mover selecionado';
$lang['Delete selected'] = 'Apagar selecionado';
$lang['Message details'] = 'Detalhes da mensagem';
$lang['Application'] = 'Aplicacao';
$lang['Open shortcut help'] = 'Abrir ajuda de atalhos';
$lang['Error'] = 'Erro';
$lang['Please specify a valid mobile phone number'] = 'Especifique um número de celular válido';
$lang['Go to {0}'] = 'Va para {0}';
$lang['Please enter a name for your message. It should be unique.'] = 'Por favor, insira um nome para sua mensagem. Deve ser unico.';
$lang['Are you sure? This will overwrite the previous message.'] = 'Tem certeza? Isso substituirá a mensagem anterior.';
$lang['PHP Frontend for gammu-smsd'] = 'Frontend PHP para gammu-smsd';
$lang['Authors'] = 'autores';
$lang['See {0} page'] = 'Veja {0} pagina';
$lang['Version'] = 'Versao';
$lang['Released'] = 'Lancada';
$lang['License'] = 'Licensa';
$lang['Homepage'] = 'Homepage';
$lang['Add a new folder'] = 'Adicione uma nova pasta';
$lang['Plugins'] = 'Plugins';
$lang['No data'] = 'Sem dados';
$lang['Select field'] = 'Selecione o campo';
$lang['Delete folder'] = 'Excluir pasta';
$lang['Role'] = 'Papel';
$lang['Delete users'] = 'Excluir usuarios';
$lang['Theme'] = 'Tema';
$lang['Background image'] = 'Imagem de fundo';
$lang['Ascending'] = 'Ascendente';
$lang['Descending'] = 'Descendente';
$lang['Value is too short.'] = 'O valor é muito curto.';
$lang['Select group name'] = 'Selecione o nome do grupo';
$lang['Uninstall'] = 'Desinstalar';
$lang['Install'] = 'Instalar';
$lang['Author'] = 'Autor';
$lang['No plugin available.'] = 'Nenhum plugin disponivel.';
$lang['No plugin installed.'] = 'Nenhum plugin instalado.';
$lang['Retrying now'] = 'Tentando novamente agora';
$lang['{0} character(s) / {1} message(s)'] = '{0} caracter(es) / {1} mensagem(ns)';
$lang['{0} message(s) deleted'] = '{0} mensagem apagada(s)';
$lang['{0} conversation(s) recovered'] = '{0} conversa(s) recuperada(s)';
$lang['Messages moved successfully'] = 'Mensagens movidas com sucesso';
$lang['Outgoing message cannot be spam'] = 'A mensagem enviada não pode ser spam';
$lang['Spam reported'] = 'Spam reportado';
$lang['Message(s) marked non-spam'] = 'Mensanges marcadas como não spam';
$lang['{0} conversation(s) deleted'] = '{0} conversa(s) excluidas';
$lang['{0} conversation(s) moved'] = '{0} conversa(s) movidas';
$lang['Value is too long.'] = 'O valor é muito longo.';
$lang['Updated'] = 'Atualizado';
$lang['Import'] = 'Importar';
$lang['Reset search'] = 'Limpar busca';
$lang['No.']['Number abbreviation'] = 'Nao.';
$lang['Insertion date'] = 'Data de inserção';
$lang['Control'] = 'Controle';
$lang['Plugin {0} is not installed.'] = 'Plugin {0} nao esta instalado.';
$lang['Value must be a number.'] = 'Valor deve ser um numero.';
$lang['{0} part messages'] = '{0} mensagens de parte';
$lang['Content'] = 'Conteudo';
$lang['No results for {0}'] = 'Sem resultados para {0}';
$lang['Failure to inject message into Gammu with gammu-smsd-inject. See kalkun logs.'] = 'Falha ao injetar mensagem no Gammu com gammu-smsd-inject. Veja os logs do kalkun.';
$lang['Group'] = 'Grupo';
$lang['Delete the original message (prevents duplicates).'] = 'Excluir a mensagem original (evita duplicatas).';
$lang['Delete permanently'] = 'Excluir permanentemente';
$lang['Retrying in {0} seconds.'] = 'Tentando novamente em {0} segundos.';
$lang['Signal'] = 'Sinal';
$lang['Battery'] = 'Bateria';
$lang['Writable'] = 'Gravavel';
$lang['Read-only'] = 'Somente leitura';
$lang['Inbox Master'] = 'Caixa de entrada';
$lang['Network error.'] = 'Erro de rede.';
$lang['{0}%'] = '{0}%';
$lang['Item deleted.'] = 'Item excluído.';
$lang['Installation has been disabled by the administrator.
To enable access to it, create a file named {0} in this directory of the server: {1}.
Otherwise you may log-in at {2}.'] = 'Instalacao foi desativada pelo administrador. Para habilitar o acesso, crie um arquivo chamado {0} neste diretorio do servidor: {1}. Caso contrario, voce pode logar em {2}.';
$lang['There is no message in this conversation.'] = 'Sem mensanges nesta conversa.';
$lang['Check again'] = 'Verifique novamente';
$lang['Password modification forbidden in demo mode.'] = 'Alteracao de senha proibida no modo de demonstração.';
$lang['Settings saved successfully (except username for kalkun user which can\'t be changed in demo mode)'] = 'Configurações salvas com sucesso (exceto nome de usuário do usuário kalkun que não pode ser alterado no modo de demonstração)';
$lang['Modification of username of "kalkun" user forbidden in demo mode. Username was restored.'] = 'Modificação do nome de usuário do usuário "kalkun" proibida no modo de demonstração. O nome de usuário foi restaurado.';
$lang['Process incoming messages'] = 'Process incoming messages';
$lang['Changing role of "kalkun" user forbidden in demo mode. Role was restored.'] = 'Changing role of "kalkun" user forbidden in demo mode. Role was restored.';
