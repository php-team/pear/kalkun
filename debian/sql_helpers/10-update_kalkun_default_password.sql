-- Update password for "kalkun" user to "kalkun"  (for version >= 0.8~dev~20200506+git065abe0)
-- if the password was still the default
UPDATE user SET password = '$2y$10$Sg0IxngRIIp1qNITM8kWa.aJ26w58F97ByTzDKoRF/dyzKcLfx226'
WHERE password = 'f0af18413d1c9e0366d8d1273160f55d5efeddfe';
