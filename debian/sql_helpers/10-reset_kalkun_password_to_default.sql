-- Reset password for "kalkun" user to "kalkun"  (for version >= 0.8~dev~20200506+git065abe0)
-- Whathever the previous password of user "kalkun"
UPDATE user SET password = '$2y$10$Sg0IxngRIIp1qNITM8kWa.aJ26w58F97ByTzDKoRF/dyzKcLfx226'
WHERE id_user = 1 AND username = 'kalkun';
